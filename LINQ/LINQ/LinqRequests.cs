﻿using LINQ.Models;
using LINQ.Models.ApiModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class LinqRequests
    {
        public static HttpClient client = new HttpClient();
        public LinqRequests()
        {
            client.BaseAddress = new Uri(Route.Global);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        //1+
        public async Task<List<CountOfTasksInProjects>> GetCountOfTaskInProjectByUserId(int Id)
        {
            var response = await client.GetAsync(Route.Ling + "getCountOfTaskInProjectByUserId/" + Id);
            var content = await response.Content.ReadAsStringAsync();
            List<CountOfTasksInProjects> result = JsonConvert.DeserializeObject<List<CountOfTasksInProjects>>(content);
            return result;
        }
        //7
        public async Task<ProjectInformation> GetByProjectId(int Id)
        {
            var response = await client.GetAsync(Route.Ling + "getByProjectId/" + Id);
            var content = await response.Content.ReadAsStringAsync();
            ProjectInformation result = JsonConvert.DeserializeObject<ProjectInformation>(content);
            return result;
        }
        //6+
        public  async Task<UserInformation> GetByUserId(int Id)
        {
            var response = await client.GetAsync(Route.Ling + "getByUserId/" + Id);
            var content = await response.Content.ReadAsStringAsync();
            UserInformation result = JsonConvert.DeserializeObject<UserInformation>(content);
            return result;
        }
        //2+
        public async Task<List<Tasks>> GetTaskOfUser(int Id)
        {
            var response = await client.GetAsync(Route.Ling + "getTasksOfUser/" + Id);
            var content = await response.Content.ReadAsStringAsync();
            List<Tasks> result = JsonConvert.DeserializeObject<List<Tasks>>(content);
            return result;
        }
        //3
        public  async Task<List<FinishedTasks>> GetFinishedTasks(int Id)
        {
            var response = await client.GetAsync(Route.Ling + "getFinishesTasks/" + Id);
            var content = await response.Content.ReadAsStringAsync();
            List<FinishedTasks> result = JsonConvert.DeserializeObject<List<FinishedTasks>>(content);
            return result;
        }
        //4+
        public  async Task<List<TeamWithUser>> GetTeamWithYoungUser()
        {
            var response = await client.GetAsync(Route.Ling + "getTeamWithYoungUsers/");
            var content = await response.Content.ReadAsStringAsync();
            List<TeamWithUser> result = JsonConvert.DeserializeObject<List<TeamWithUser>>(content);
            return result;
        }
        //5+
        public  async Task<List<UserWithTasks>> GetSortedListByUserAndTeam()
        {
            var response = await client.GetAsync(Route.Ling + "getSortedList/");
            var content = await response.Content.ReadAsStringAsync();
            List<UserWithTasks> result = JsonConvert.DeserializeObject<List<UserWithTasks>>(content);
            return result;
        }
    }
}
