﻿using Castle.Core;
using LINQ.Models;
using LINQ.Models.ApiModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        public static HttpClient client = new HttpClient();
        public static LinqRequests requests = new LinqRequests();
        public static ShowLinqRequest showRequests = new ShowLinqRequest();
        public static CrudMenu menu = new CrudMenu();
        static async Task MenuAsync()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine("\t1- Request 1\n" +
                "\t2- Request 2\n" +
                "\t3- Request 3\n" +
                "\t4- Request 4\n" +
                "\t5- Request 5\n" +
                "\t6- Request 6\n" +
                "\t7- Request 7\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request1 = await requests.GetCountOfTaskInProjectByUserId(enterNumber);
                    showRequests.ShowCountOfTaskInProjectByUserId(request1);
                    break;
                case 2:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request2 = await requests.GetTaskOfUser(enterNumber);
                    showRequests.ShowTaskOfUser(request2);
                    break;
                case 3:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request3 = await requests.GetFinishedTasks(enterNumber);
                    showRequests.ShowFinishedTasks(enterNumber, request3);
                    break;
                case 4:
                    var request4 = await requests.GetTeamWithYoungUser();
                    showRequests.ShowTeamWithYoungUser(request4);
                    break;
                case 5:
                    var request5 = await requests.GetSortedListByUserAndTeam();
                    showRequests.ShowSortedListByUserAndTeam(request5);
                    break;
                case 6:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request6 = await requests.GetByUserId(enterNumber);
                    showRequests.ShowByUserId(request6);
                    break;
                case 7:
                    Console.WriteLine("Enter project Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var request7 = await requests.GetByProjectId(enterNumber);
                    showRequests.ShowByProjectId(request7);
                    break;
            }
        }
     
        static async Task CreateMenu()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine("\t1- CRUD with Project\n" +
                "\t2- CRUD with Task\n" +
                "\t3- CRUD with Team\n" +
                "\t4- CRUD with User\n" +
                "\t5- CRUD with TaskState\n" +
                "\t6- Linq Request\n");
            number = Int32.Parse(Console.ReadLine());
            switch (number)
            {
                case 1:
                    await menu.CrudProjectMenu();
                    break;
                case 2:
                    await menu.CrudTaskMenu();
                    break;
                case 3:
                    await menu.CrudTeamMenu();
                    break;
                case 4:
                    await menu.CrudUserMenu();
                    break;
                case 5:
                    await menu.CrudTaskStateMenu();
                    break;
                case 6:
                    await MenuAsync();
                    break;
            }
        }

        static async Task Run()
        {
            await CreateMenu();
        }

        static void Main(string[] args)
        {
            Run().GetAwaiter().GetResult(); ;
        }
    }
}
