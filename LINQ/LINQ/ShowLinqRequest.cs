﻿using LINQ.Models;
using LINQ.Models.ApiModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class ShowLinqRequest
    {
        //1+
        public  void ShowCountOfTaskInProjectByUserId (List<CountOfTasksInProjects> result)
        {
            Console.WriteLine($"Count of task in project:");
            foreach (var item in result)
            {
                Console.WriteLine($"\tName: {item.project.name} , Count: {item.countOfTasks}");
            }
            Console.WriteLine();
        }
        //7
        public void ShowByProjectId(ProjectInformation result)
        {
            Console.WriteLine($"Project:\n {result.project}\n" +
                $" TaskWithLongestDescrition:\n {result.taskWithLongestDescrition}\n" +
                $" TaskWithShortestName:\n {result.taskWithShortestName}\n" +
                $" Count of user in team: {result.countOfUser}\n");
        }
        //6+
        public void ShowByUserId(UserInformation result)
        {
            Console.WriteLine(result.user);
            Console.WriteLine("\tMin Project: \n" + result.minProject);
            Console.WriteLine("\tCount of tasks in Min Project:" + result.countOfTaskInMinProject);
            Console.WriteLine("\tCount of Canceled or Not finished tasks: " + result.countOfCanceledOrNotFinishedTasks);
            Console.WriteLine("\tLongest user`s task: \n" + result.longestTask);
        }
        //2+
        public void ShowTaskOfUser(List<Tasks> result)
        {
            Console.WriteLine("Tasks:");
            foreach (var item in result)
            {
                Console.WriteLine("\t" + item);
            }
        }
        //3
        public void ShowFinishedTasks(int Id, List<FinishedTasks> result)
        {
            Console.WriteLine($"Lit of tasks that was finished in 2019 by perfomer with id {Id}");
            foreach (var item in result)
            {
                Console.WriteLine($"\tId: {item.id} , Name: {item.name}");
            }
        }
        //4+
        public void ShowTeamWithYoungUser(List<TeamWithUser> result)
        {
            Console.WriteLine("List of teams and their participants who are older than 12 years old, sorted by the date of registration of the user in descending order, and grouped by teams.");
            foreach (var item in result)
            {
                Console.WriteLine($"Id: {item.id}, Name of Team: {item.name}");
                Console.WriteLine("List of participants:");
                foreach (var user in item.users)
                {
                    Console.WriteLine("\t" + user);
                }
            }
        }
        //5+
        public void ShowSortedListByUserAndTeam(List<UserWithTasks> result)
        {
            foreach (var user in result)
            {
                Console.WriteLine(user.user);
                Console.WriteLine("List of tasks");
                foreach (var task in user.tasks)
                {
                    Console.WriteLine("\t" + task);
                }
                Console.WriteLine("===========================================================");
            }
        }
    }
}
