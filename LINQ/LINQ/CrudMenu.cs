﻿using LINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class CrudMenu
    {
        public static HttpClient client = new HttpClient();
        public CrudMenu()
        {
            client.BaseAddress = new Uri(Route.Global);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task CrudProjectMenu()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine($"\t1- Create Project 1\n" +
                $"\t2- Read Project by Id 2\n" +
                $"\t3- Read all Project 2\n" +
                $"\t4- Update Project 3\n" +
                $"\t5- Delete Project 4\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter Project name:");
                    string newName = Console.ReadLine();
                    Console.WriteLine("Enter Project description:");
                    string newDescription = Console.ReadLine();
                    Console.WriteLine("Enter Project deadline:");
                    DateTime newDeadline = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project author id:");
                    int newAuthorId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project team id:");
                    int newTeamId = Int32.Parse(Console.ReadLine());
                    Project newProject = new Project
                    {
                        name = newName,
                        description = newDescription,
                        deadline = newDeadline,
                        created_at = DateTime.Now,
                        team_id = newTeamId,
                        author_id = newAuthorId
                    };
                    var result = JsonConvert.SerializeObject(newProject);
                    var response1 = await client.PostAsJsonAsync(Route.Projects, result);
                    break;
                case 2:
                    Console.WriteLine("Enter Project Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response2 = await client.GetAsync(Route.Projects + enterNumber);
                    var content = await response2.Content.ReadAsStringAsync();
                    Console.WriteLine(content);
                    break;
                case 3:
                    var response3 = await client.GetAsync(Route.Projects);
                    var content3 = JsonConvert.DeserializeObject<List<Project>>(await response3.Content.ReadAsStringAsync());
                    foreach (var item in content3)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 4:
                    Console.WriteLine("Enter Project Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project name:");
                    string _newName = Console.ReadLine();
                    Console.WriteLine("Enter Project description:");
                    string _newDescription = Console.ReadLine();
                    Console.WriteLine("Enter Project deadline:");
                    DateTime _newDeadline = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project author id:");
                    int _newAuthorId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project team id:");
                    int _newTeamId = Int32.Parse(Console.ReadLine());
                    Project _newProject = new Project
                    {
                        id = enterNumber,
                        name = _newName,
                        description = _newDescription,
                        deadline = _newDeadline,
                        team_id = _newTeamId,
                        author_id = _newAuthorId
                    };
                    var result4 = JsonConvert.SerializeObject(_newProject);
                    var response4 = await client.PutAsJsonAsync(Route.Projects, result4);
                    break;
                case 5:
                    Console.WriteLine("Enter Project Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response5 = await client.DeleteAsync(Route.Projects + enterNumber);
                    break;
            }
        }

        public  async Task CrudTaskMenu()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine($"\t1- Create Task 1\n" +
                $"\t2- Read Task by Id 2\n" +
                $"\t3- Read all Task 2\n" +
                $"\t4- Update Task 3\n" +
                $"\t5- Delete Task 4\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter Task name:");
                    string newName = Console.ReadLine();
                    Console.WriteLine("Enter Task description:");
                    string newDescription = Console.ReadLine();
                    Console.WriteLine("Enter Task state:");
                    int newstate = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Task perfomer id:");
                    int newPerfomerId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Task project id:");
                    int newProjectId = Int32.Parse(Console.ReadLine());
                    Tasks newTask = new Tasks
                    {
                        name = newName,
                        description = newDescription,
                        state = newstate,
                        project_id = newProjectId,
                        performer_id = newPerfomerId
                    };
                    var result = JsonConvert.SerializeObject(newTask);
                    var response1 = await client.PostAsJsonAsync(Route.Tasks, result);
                    break;
                case 2:
                    Console.WriteLine("Enter Task Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response2 = await client.GetAsync(Route.Tasks + enterNumber);
                    var content = await response2.Content.ReadAsStringAsync();
                    Console.WriteLine(content);
                    break;
                case 3:
                    var response3 = await client.GetAsync(Route.Tasks);
                    var content3 = JsonConvert.DeserializeObject<List<Tasks>>(await response3.Content.ReadAsStringAsync());
                    foreach (var item in content3)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 4:
                    Console.WriteLine("Enter Task Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Task name:");
                    string _newName = Console.ReadLine();
                    Console.WriteLine("Enter Task description:");
                    string _newDescription = Console.ReadLine();
                    Console.WriteLine("Enter Task state:");
                    int _newstate = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Task perfomer id:");
                    int _newPerfomerId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Task project id:");
                    int _newProjectId = Int32.Parse(Console.ReadLine());
                    Tasks _newTask = new Tasks
                    {
                        id = enterNumber,
                        name = _newName,
                        description = _newDescription,
                        state = _newstate,
                        project_id = _newProjectId,
                        performer_id = _newPerfomerId
                    };
                    var result4 = JsonConvert.SerializeObject(_newTask);
                    var response4 = await client.PutAsJsonAsync(Route.Tasks, result4);
                    break;
                case 5:
                    Console.WriteLine("Enter Task Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response5 = await client.DeleteAsync(Route.Tasks + enterNumber);
                    break;
            }
        }

        public  async Task CrudTeamMenu()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine($"\t1- Create Team 1\n" +
                $"\t2- Read Team by Id 2\n" +
                $"\t3- Read all Team 2\n" +
                $"\t4- Update Team 3\n" +
                $"\t5- Delete Team 4\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter Team name:");
                    string newName = Console.ReadLine();
                    Team newTeam = new Team
                    {
                        name = newName,
                        created_at = DateTime.Now
                    };
                    var result = JsonConvert.SerializeObject(newTeam);
                    var response1 = await client.PostAsJsonAsync(Route.Teams, result);
                    break;
                case 2:
                    Console.WriteLine("Enter Team Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response2 = await client.GetAsync(Route.Teams + enterNumber);
                    var content = await response2.Content.ReadAsStringAsync();
                    Console.WriteLine(content);
                    break;
                case 3:
                    var response3 = await client.GetAsync(Route.Teams);
                    var content3 = JsonConvert.DeserializeObject<List<Team>>(await response3.Content.ReadAsStringAsync());
                    foreach (var item in content3)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 4:
                    Console.WriteLine("Enter Team Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Team name:");
                    string _newName = Console.ReadLine();
                    Team _newTeam = new Team
                    {
                        id = enterNumber,
                        name = _newName,
                    };
                    var result4 = JsonConvert.SerializeObject(_newTeam);
                    var response4 = await client.PutAsJsonAsync(Route.Teams, result4);
                    break;
                case 5:
                    Console.WriteLine("Enter Team Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response5 = await client.DeleteAsync(Route.Teams + enterNumber);
                    break;
            }
        }

        public  async Task CrudUserMenu()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine($"\t1- Create User 1\n" +
                $"\t2- Read User by Id 2\n" +
                $"\t3- Read all User 2\n" +
                $"\t4- Update User 3\n" +
                $"\t5- Delete User 4\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter User first name:");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("Enter User last name:");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("Enter User birthday:");
                    DateTime newBirthday = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project team id:");
                    int newTeamId = Int32.Parse(Console.ReadLine());
                    User newUser = new User
                    {
                        first_name = firstName,
                        last_name = lastName,
                        birthday = newBirthday,
                        registered_at = DateTime.Now,
                        team_id = newTeamId,
                    };
                    var result = JsonConvert.SerializeObject(newUser);
                    var response1 = await client.PostAsJsonAsync(Route.Users, result);
                    break;
                case 2:
                    Console.WriteLine("Enter User Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response2 = await client.GetAsync(Route.Users + enterNumber);
                    var content = await response2.Content.ReadAsStringAsync();
                    Console.WriteLine(content);
                    break;
                case 3:
                    var response3 = await client.GetAsync(Route.Users);
                    var content3 = JsonConvert.DeserializeObject<List<User>>(await response3.Content.ReadAsStringAsync());
                    foreach (var item in content3)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 4:
                    Console.WriteLine("Enter User Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter User first name:");
                    string _firstName = Console.ReadLine();
                    Console.WriteLine("Enter User last name:");
                    string _lastName = Console.ReadLine();
                    Console.WriteLine("Enter User birthday:");
                    DateTime _newBirthday = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Enter Project team id:");
                    int _newTeamId = Int32.Parse(Console.ReadLine());
                    User _newUser = new User
                    {
                        id = enterNumber,
                        first_name = _firstName,
                        last_name = _lastName,
                        birthday = _newBirthday,
                        registered_at = DateTime.Now,
                        team_id = _newTeamId,
                    };
                    var result4 = JsonConvert.SerializeObject(_newUser);
                    var response4 = await client.PutAsJsonAsync(Route.Users, result4);
                    break;
                case 5:
                    Console.WriteLine("Enter User Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response5 = await client.DeleteAsync(Route.Users + enterNumber);
                    break;
            }
        }

        public  async Task CrudTaskStateMenu()
        {
            int number = 0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine($"\t1- Create TaskState 1\n" +
                $"\t2- Read TaskState by Id 2\n" +
                $"\t3- Read all TaskState 2\n" +
                $"\t4- Update TaskState 3\n" +
                $"\t5- Delete TaskState 4\n");
            number = Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter TaskState value:");
                    string value = Console.ReadLine();
                    TaskStateModel newTaskState = new TaskStateModel
                    {
                        value = value
                    };
                    var result = JsonConvert.SerializeObject(newTaskState);
                    var response1 = await client.PostAsJsonAsync(Route.States, result);
                    break;
                case 2:
                    Console.WriteLine("Enter TaskState Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response2 = await client.GetAsync(Route.States + enterNumber);
                    var content = await response2.Content.ReadAsStringAsync();
                    Console.WriteLine(content);
                    break;
                case 3:
                    var response3 = await client.GetAsync(Route.States);
                    var content3 = JsonConvert.DeserializeObject<List<TaskStateModel>>(await response3.Content.ReadAsStringAsync());
                    foreach (var item in content3)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 4:
                    Console.WriteLine("Enter TaskState Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Enter TaskState value:");
                    string _value = Console.ReadLine();
                    TaskStateModel _newTaskState = new TaskStateModel
                    {
                        id = enterNumber,
                        value = _value
                    };
                    var result4 = JsonConvert.SerializeObject(_newTaskState);
                    var response4 = await client.PutAsJsonAsync(Route.States, result4);
                    break;
                case 5:
                    Console.WriteLine("Enter TaskState Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    var response5 = await client.DeleteAsync(Route.States + enterNumber);
                    break;
            }
        }
    }
}
