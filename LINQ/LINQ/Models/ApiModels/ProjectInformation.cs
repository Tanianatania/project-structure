﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models.ApiModels
{
    public class ProjectInformation
    {
        public Project project;
        public Tasks taskWithLongestDescrition;
        public Tasks taskWithShortestName;
        public int countOfUser;
    }
}
