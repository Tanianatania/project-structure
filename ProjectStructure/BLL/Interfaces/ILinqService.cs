﻿using BLL.DTO;
using BLL.DTO.LinqModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ILinqService
    {
        IEnumerable<CountOfTasksInProjectsDTO>  GetCountOfTaskInProjectByUserId(int Id);
        ProjectInformationDTO GetByProjectId(int Id);
        UserInformationDTO GetByUserId(int Id);
        IEnumerable<TasksDTO> GetTaskOfUser(int Id);
        IEnumerable<FinishedTasksDTO> GetFinishedTasks(int Id);
        IEnumerable<TeamWithUserDTO> GetTeamWithYoungUser();
        IEnumerable<UserWithTasksDTO> GetSortedListByUserAndTeam();
    }
}
