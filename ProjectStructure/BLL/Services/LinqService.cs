﻿using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class LinqService : ILinqService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Tasks> _taskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Team> _teamRepository;

        private List<TasksDTO> _tasks;
        private List<ProjectDTO> _projects;
        private List<UserDTO> _users;
        private List<TeamDTO> _teams;

        public LinqService(IRepository<Project> projectRepository,
            IRepository<Tasks> taskRepository,
            IRepository<User> userRepository,
            IRepository<Team> teamRepository,
            IMapper mapper)
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _mapper = mapper;
            _tasks = _taskRepository.GetList().Select(item => _mapper.Map<Tasks, TasksDTO>(item)).ToList();
            _projects = _projectRepository.GetList().Select(item => _mapper.Map<Project, ProjectDTO>(item)).ToList();
            _users=_userRepository.GetList().Select(item => _mapper.Map<User, UserDTO>(item)).ToList();
            _teams=_teamRepository.GetList().Select(item => _mapper.Map<Team, TeamDTO>(item)).ToList();
        }
        public IEnumerable<CountOfTasksInProjectsDTO> GetCountOfTaskInProjectByUserId(int Id)
        {
            var result = from project in _projects
                         join task in _tasks on project.Id equals task.Project_id
                         where task.Performer_id == Id
                         group task by project into ress
                         select new CountOfTasksInProjectsDTO { Project=ress.Key, CountOfTasks = ress.Count() };
            return result.ToList();
        }
        public ProjectInformationDTO GetByProjectId(int Id)
        {
            var result = (from project in _projects
                         join user in _users on project.Team_id equals user.Team_id
                         where project.Id == Id
                         group user by project into ress
                         select new ProjectInformationDTO
                         {
                             Project = _projects.Where(a => a.Id == Id).FirstOrDefault(),
                             TaskWithLongestDescrition = _tasks.Where(a => a.Project_id == Id).OrderByDescending(a => a.Description.Length).FirstOrDefault(),
                             TaskWithShortestName = _tasks.Where(a => a.Project_id == Id).OrderBy(a => a.Name.Length).FirstOrDefault(),
                             CountOfUser = ress.Count()
                         }).FirstOrDefault();
            return result;
        }
        public UserInformationDTO GetByUserId(int Id)
        {
            var result = (from user in _users
                         where user.Id == Id
                         select new UserInformationDTO
                         {
                             User=user,
                             MinProject = _projects.Where(a => a.Author_id == Id).OrderByDescending(a => a.Created_at).First(),
                             CountOfTaskInMinProject = _tasks.Where(a => a.Project_id == (_projects.Where(b => b.Author_id == Id).OrderByDescending(b => b.Created_at).First().Id)).Count(),
                             CountOfCanceledOrNotFinishedTasks = _tasks.Where(a => a.Performer_id == Id && (a.State == 2 || a.State == 4)).Count(),
                             LongestTask = _tasks.Where(a => a.Performer_id == Id).OrderByDescending(a => (a.Finished_at - a.Created_at)).First()
                         }).FirstOrDefault();
            return result;
        }
        public IEnumerable<FinishedTasksDTO> GetFinishedTasks(int Id)
        {
            var result = _tasks.Where(a => a.Performer_id == Id && a.Finished_at.Year == 2019 && a.State==3).Select(a => new FinishedTasksDTO { Id=a.Id, Name=a.Name }).ToList();
            return result;
        }
        public IEnumerable<UserWithTasksDTO> GetSortedListByUserAndTeam()
        {
            var result = from user in _users
                         join task in _tasks on user.Id equals task.Performer_id
                         orderby user.First_name
                         group task by user into ress
                         select new UserWithTasksDTO{ User=ress.Key, Tasks = ress.OrderByDescending(a => a.Name.Length).ToList() };
            return result;
        }
        public IEnumerable<TasksDTO> GetTaskOfUser(int Id)
        {
            var result = _tasks.Where(a => a.Performer_id == Id && a.Name.Length < 45).ToList();
            return result;
        }
        public IEnumerable<TeamWithUserDTO> GetTeamWithYoungUser()
        {
            var result = (from team in _teams
                         join user in _users on team.Id equals user.Team_id
                         orderby user.Registered_at
                         group user by team into ress
                         where ress.All(a => DateTime.Now.Year - a.Birthday.Year >= 12)
                         select new TeamWithUserDTO
                         {
                             Id = ress.Key.Id,
                             Name = ress.Key.Name,
                             Users = ress.ToList()
                         });
            return result;
        }
    }
}
