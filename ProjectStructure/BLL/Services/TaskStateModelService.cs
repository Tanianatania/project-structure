﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;

namespace BLL.Services
{
    public class TaskStateModelService : IService<TaskStateModelDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<TaskStateModel> _repository;

        public TaskStateModelService(IRepository<TaskStateModel> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Create(TaskStateModelDTO item)
        {
            var taskState = _mapper.Map<TaskStateModelDTO, TaskStateModel>(item);
            _repository.Create(taskState);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public TaskStateModelDTO Get(int id)
        {
            return _mapper.Map<TaskStateModel, TaskStateModelDTO>(_repository.Get(id));
        }

        public IEnumerable<TaskStateModelDTO> GetList()
        {
            return _repository.GetList().Select(item => _mapper.Map<TaskStateModel, TaskStateModelDTO>(item));
        }

        public void Update(TaskStateModelDTO item)
        {
            var taskState = _mapper.Map<TaskStateModelDTO, TaskStateModel>(item);
            _repository.Update(taskState);
        }
    }
}
