﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;

namespace BLL.Services
{
    public class TeamService : IService<TeamDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Team> _repository;

        public TeamService(IRepository<Team> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Create(TeamDTO item)
        {
            var team = _mapper.Map<TeamDTO, Team>(item);
            _repository.Create(team);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public TeamDTO Get(int id)
        {
            return _mapper.Map<Team, TeamDTO>(_repository.Get(id));
        }

        public IEnumerable<TeamDTO> GetList()
        {
            return _repository.GetList().Select(item => _mapper.Map<Team, TeamDTO>(item));
        }

        public void Update(TeamDTO item)
        {
            var team = _mapper.Map<TeamDTO, Team>(item);
            _repository.Update(team);
        }
    }
}
