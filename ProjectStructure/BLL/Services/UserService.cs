﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;

namespace BLL.Services
{
    public class UserService : IService<UserDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<User> _repository;

        public UserService(IRepository<User> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Create(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);
            _repository.Create(user);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public UserDTO Get(int id)
        {
            return _mapper.Map<User, UserDTO>(_repository.Get(id));
        }

        public IEnumerable<UserDTO> GetList()
        {
            return _repository.GetList().Select(item => _mapper.Map<User, UserDTO>(item));
        }

        public void Update(UserDTO item)
        {
            var user = _mapper.Map<UserDTO, User>(item);
            _repository.Update(user);
        }
    }
}
