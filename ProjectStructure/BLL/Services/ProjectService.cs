﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;

namespace BLL.Services
{
    public class ProjectService : IService<ProjectDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Project> _repository;

        public ProjectService(IRepository<Project> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Create(ProjectDTO item)
        {
            var project = _mapper.Map<ProjectDTO, Project>(item);
            _repository.Create(project);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public ProjectDTO Get(int id)
        {
            return _mapper.Map<Project,ProjectDTO>(_repository.Get(id));
        }

        public IEnumerable<ProjectDTO> GetList()
        {
            return _repository.GetList().Select(item=>_mapper.Map<Project,ProjectDTO>(item));
        }

        public void Update(ProjectDTO item)
        {
            var project = _mapper.Map<ProjectDTO, Project>(item);
            _repository.Update(project);
        }
    }
}
