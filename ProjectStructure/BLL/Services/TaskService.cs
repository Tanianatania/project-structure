﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;

namespace BLL.Services
{
    public class TaskService : IService<TasksDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Tasks> _repository;

        public TaskService(IRepository<Tasks> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Create(TasksDTO item)
        {
            var task = _mapper.Map<TasksDTO, Tasks>(item);
            _repository.Create(task);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public TasksDTO Get(int id)
        {
            return _mapper.Map<Tasks, TasksDTO>(_repository.Get(id));
        }

        public IEnumerable<TasksDTO> GetList()
        {
            return _repository.GetList().Select(item => _mapper.Map<Tasks, TasksDTO>(item));
        }

        public void Update(TasksDTO item)
        {
            var task = _mapper.Map<TasksDTO, Tasks>(item);
            _repository.Update(task);
        }
    }
}
