﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime deadline { get; set; }
        public int Author_id { get; set; }
        public int Team_id{ get; set; }
        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Description: {Description.Replace("\n", " ")}, Create at: {Created_at}, Deadline: {deadline}, " +
                $"Author id: {Author_id}, Team id: {Team_id}\n";
        }
    }
}
