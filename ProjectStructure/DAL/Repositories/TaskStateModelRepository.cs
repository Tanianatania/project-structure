﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class TaskStateModelRepository : IRepository<TaskStateModel>
    {
        private readonly List<TaskStateModel> _taskState;
        DataSource dataSource = new DataSource();

        public TaskStateModelRepository()
        {
            _taskState = dataSource.GetAllTaskStateModels();
        }

        public IEnumerable<TaskStateModel> GetList()
        {
            return _taskState;
        }

        public TaskStateModel Get(int id)
        {
            return _taskState.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(TaskStateModel item)
        {
            if (_taskState.Count != 0)
            {
                var _lastProject = _taskState.Last();
                item.Id = _lastProject.Id + 1;
            }
            _taskState.Add(item);
        }

        public void Update(TaskStateModel item)
        {
            var oldTasks = _taskState.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _taskState.Remove(oldTasks);
                _taskState.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _taskState.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _taskState.Remove(oldTasks);
            }
        }
    }
}
