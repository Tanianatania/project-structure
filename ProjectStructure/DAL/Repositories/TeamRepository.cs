﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly List<Team> _teams;
        DataSource dataSource = new DataSource();

        public TeamRepository()
        {
            _teams = dataSource.GetTeams();
        }

        public IEnumerable<Team> GetList()
        {
            return _teams;
        }

        public Team Get(int id)
        {
            return _teams.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(Team item)
        {
            if (_teams.Count != 0)
            {
                var _lastProject = _teams.Last();
                item.Id = _lastProject.Id + 1;
            }
            _teams.Add(item);
        }

        public void Update(Team item)
        {
            var oldTasks = _teams.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _teams.Remove(oldTasks);
                _teams.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _teams.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _teams.Remove(oldTasks);
            }
        }
    }
}
