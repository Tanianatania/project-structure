﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repositories
{
    public class TaskRepository : IRepository<Tasks>
    {
        private readonly List<Tasks> _tasks;
        DataSource dataSource = new DataSource();

        public TaskRepository()
        {
            _tasks = dataSource.GetTasks();
        }

        public IEnumerable<Tasks> GetList()
        {
            return _tasks;
        }

        public Tasks Get(int id)
        {
            return _tasks.Where(a => a.Id == id).FirstOrDefault();
        }

        public void Create(Tasks item)
        {
            if (_tasks.Count != 0)
            {
                var _lastProject = _tasks.Last();
                item.Id = _lastProject.Id + 1;
            }
            _tasks.Add(item);
        }

        public void Update(Tasks item)
        {
            var oldTasks = _tasks.Where(a => a.Id == item.Id).FirstOrDefault();
            if (oldTasks != null)
            {
                _tasks.Remove(oldTasks);
                _tasks.Add(item);
            }
        }

        public void Delete(int id)
        {
            var oldTasks = _tasks.Where(a => a.Id == id).FirstOrDefault();
            if (oldTasks != null)
            {
                _tasks.Remove(oldTasks);
            }
        }
    }
}

