﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class TaskStateModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
