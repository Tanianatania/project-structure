﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Tasks
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Finished_at { get; set; }
        public int State { get; set; }
        public int Project_id { get; set; }
        public int Performer_id { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Description: {Description.Replace("\n"," ")}, Create at: {Created_at}, Finished at: {Finished_at}" +
                $" State: {State}, Project Id: {Project_id}, Perfomer Id: {Performer_id}\n";
        }
    }
}
