﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TasksDTO> _service;
        private readonly IMapper _mapper;

        public TasksController(IService<TasksDTO> service,
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        // GET: api/Tasks
        [HttpGet]
        public IEnumerable<TasksViewModel> Get()
        {
            return _service.GetList().Select(item => _mapper.Map<TasksDTO, TasksViewModel>(item));
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTasks")]
        public TasksViewModel Get(int id)
        {
            return _mapper.Map<TasksDTO, TasksViewModel>(_service.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public void Post([FromBody] string value)
        {
            TasksViewModel item = JsonConvert.DeserializeObject<TasksViewModel>(value);
            _service.Create(_mapper.Map<TasksViewModel, TasksDTO>(item));
        }

        // PUT: api/Tasks
        [HttpPut]
        public void Put([FromBody] string value)
        {
            TasksViewModel item = JsonConvert.DeserializeObject<TasksViewModel>(value);
            _service.Update(_mapper.Map<TasksViewModel, TasksDTO>(item));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
