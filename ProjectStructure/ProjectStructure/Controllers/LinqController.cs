﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Models.LinqModels;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly ILinqService _service;
        private readonly IMapper _mapper;

        public LinqController(ILinqService service,
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [Route("getCountOfTaskInProjectByUserId/{id}")]
        [HttpGet]
        public IEnumerable<CountOfTasksInProjectsViewModel> GetCountOfTaskInProjectByUserId(int id)
        {
            return _service.GetCountOfTaskInProjectByUserId(id).Select(item=> _mapper.Map<CountOfTasksInProjectsDTO, CountOfTasksInProjectsViewModel>(item));
        }

        [Route("getByProjectId/{id}")]
        [HttpGet]
        public ProjectInformationViewModel GetByProjectId(int id)
        {
            return _mapper.Map<ProjectInformationDTO, ProjectInformationViewModel>(_service.GetByProjectId(id));
        }

        [Route("getByUserId/{id}")]
        [HttpGet]
        public UserInformationViewModel GetByUserId(int id)
        {
           return _mapper.Map<UserInformationDTO, UserInformationViewModel>(_service.GetByUserId(id));
        }

        [Route("getFinishesTasks/{id}")]
        [HttpGet]
        public IEnumerable<FinishedTasksViewModel> GetFinishedTasks(int id)
        {
           return  _service.GetFinishedTasks(id).Select(item=>_mapper.Map<FinishedTasksDTO, FinishedTasksViewModel>(item));
        }

        [Route("getSortedList")]
        [HttpGet]
        public IEnumerable<UserWithTasksViewModel> GetSortedListByUserAndTeam()
        {
            return _service.GetSortedListByUserAndTeam().Select(item => _mapper.Map<UserWithTasksDTO, UserWithTasksViewModel>(item));
        }

        [Route("getTasksOfUser/{id}")]
        [HttpGet]
        public IEnumerable<TasksViewModel> GetTaskOfUser(int id)
        {
            return _service.GetTaskOfUser(id).Select(item => _mapper.Map<TasksDTO, TasksViewModel>(item));
        }

        [Route("getTeamWithYoungUsers")]
        [HttpGet]
        public IEnumerable<TeamWithUserViewModel> GetTeamWithYoungUsers()
        {
            return _service.GetTeamWithYoungUser().Select(item=> _mapper.Map<TeamWithUserDTO, TeamWithUserViewModel>(item));
        }
    }
}