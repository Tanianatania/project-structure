﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _service;
        private readonly IMapper _mapper;

        public ProjectsController(IService<ProjectDTO> service,
            IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        // GET: api/Projects
        [HttpGet]
        public IEnumerable<ProjectViewModel> Get()
        {
            return _service.GetList().Select(item => _mapper.Map<ProjectDTO, ProjectViewModel>(item));
        }

        // GET: api/Projects/5
        [HttpGet("{id}", Name = "GetProject")]
        public ProjectViewModel Get(int id)
        {
            return _mapper.Map<ProjectDTO, ProjectViewModel>(_service.Get(id));
        }

        // POST: api/Projects
        [HttpPost]
        public void Post([FromBody] string value)
        {
            ProjectViewModel item = JsonConvert.DeserializeObject<ProjectViewModel>(value);
            _service.Create(_mapper.Map<ProjectViewModel, ProjectDTO>(item));
        }

        // PUT: api/Projects/5
        [HttpPut]
        public void Put([FromBody] string value)
        {
            ProjectViewModel item = JsonConvert.DeserializeObject<ProjectViewModel>(value);
            _service.Update(_mapper.Map<ProjectViewModel, ProjectDTO>(item));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
