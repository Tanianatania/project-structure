﻿using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using DAL.Entities;
using ProjectStructure.Models.LinqModels;

namespace ProjectStructure.Models.Profilies
{
    public class LinqProfile:Profile
    {
        public LinqProfile()
        {
            CreateMap<CountOfTasksInProjectsDTO, CountOfTasksInProjectsViewModel>();
            CreateMap<CountOfTasksInProjectsViewModel, CountOfTasksInProjectsDTO>();

            CreateMap<FinishedTasksDTO, FinishedTasksViewModel>();
            CreateMap<FinishedTasksViewModel, FinishedTasksDTO>();

            CreateMap<ProjectInformationDTO, ProjectInformationViewModel>();
            CreateMap<ProjectInformationViewModel, ProjectInformationDTO>();

            CreateMap<TeamWithUserViewModel, TeamWithUserDTO>();
            CreateMap<TeamWithUserDTO, TeamWithUserViewModel>();

            CreateMap<UserInformationDTO, UserInformationViewModel>();
            CreateMap<UserInformationDTO, UserInformationViewModel>();

            CreateMap<UserWithTasksDTO, UserWithTasksViewModel>();
            CreateMap<UserWithTasksViewModel, TeamWithUserDTO>();
        }
    }
}
