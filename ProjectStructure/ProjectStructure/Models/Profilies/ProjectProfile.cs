﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace ProjectStructure.Models.Profilies
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>();
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, ProjectViewModel>();
            CreateMap<ProjectViewModel, ProjectDTO>();
        }
    }
}
