﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace ProjectStructure.Models.Profilies
{
    public class TaskStateModelProfile : Profile
    {
        public TaskStateModelProfile()
        {
            CreateMap<TaskStateModel, TaskStateModelDTO>();
            CreateMap<TaskStateModelDTO, TaskStateModel>();
            CreateMap<TaskStateViewModel, TaskStateModelDTO>();
            CreateMap<TaskStateModelDTO, TaskStateViewModel>();
        }
    }
}