﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace ProjectStructure.Models.Profilies
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, TeamViewModel>();
            CreateMap<TeamViewModel, TeamDTO>();

        }
    }
}
