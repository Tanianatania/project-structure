﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models
{
   public class TeamViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_at { get; set; }
    }
}
