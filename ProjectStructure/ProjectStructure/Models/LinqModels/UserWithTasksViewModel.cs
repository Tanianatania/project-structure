﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models.LinqModels
{
    public class UserWithTasksViewModel
    {
        public UserViewModel User { get; set; }
        public List<TasksViewModel> Tasks { get; set; }
    }
}
