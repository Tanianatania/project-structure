﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Models
{
    public class TaskStateViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
